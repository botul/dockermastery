# DockerMastery  

$ docker version  
$ docker info  
$ docker container ls  
$ docker container ls -a  
$ docker container run --publish 80:80 --detach nginx  

$ docker container run --publish 80:80 --detach --name webhost nginx  
$ docker container logs webhost  
$ docker container top webhost  
$ docker run --name mongo -d mongo  
$ docker top mongo  
$ docker ps  
$ ps aux |grep mongo  

Running and managing multiple containers  
$ docker container run -d -p 3306:3306 --name db -e MYSQL_RANDOM_ROOT_PASSWORD=yes mysql  
$ docker container logs db  
$ docker container run -d --name webserver -p 8080:80 httpd  
$ docker ps  
$ docker container run -d --name proxy -p 80:80 nginx  

Process monitoring  
$ docker container run -d --name nginx nginx  
$ docker container run -d -p 3306:3306 --name mysql -e MYSQL_RANDOM_ROOT_PASSWORD=yes mysql  
$ docker container ls  

$ docker container top nginx // - process list in one container  
$ docker container top mysql  
$ docker container inspect mysql // - details of one container config  

$ docker container stats // - performance stats for all cont  

Getting shell insode containers:  
$ docker container run -it - start new container interactively  
$ docker container exec -it - run additional command in existing container  

$docker container exec -it mysql bash  

Running own Docker Registry:  

$ docker container run -d -p 5000:5000 --name registry registry  

$ docker pull hello-world  
$ docker tag hello-world 127.0.0.1:5000/hello-world  

$ docker push 127.0.0.1:5000/hello-world  

Remove existing images from local cache:  
$ docker image rm hello-world  
$ docker image rm 127.0.0.1:5000/hello-world  

Pulling image from local registry:  
$ docker pull 127.0.0.1:5000/hello-world  

Create registry with bind mount:  
$ docker container run -d -p 5000:5000 --name registry -v $(pwd)/registry-data:/var/lib/registry registry  

